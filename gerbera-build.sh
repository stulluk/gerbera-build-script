#!/bin/bash

set -e

rm -rf build gerbera

sudo aptitude install libssl-dev

sudo apt install software-properties-common -y

sudo add-apt-repository ppa:ubuntu-toolchain-r/test -y

sudo apt-get update

sudo apt-get -y install autoconf g++ subversion \
	linux-source build-essential tofrodos \
	git-core subversion dos2unix make gcc \
	automake cmake3 checkinstall git-core \
	dpkg-dev fakeroot pbuilder dh-make \
	debhelper devscripts patchutils quilt \
	git-buildpackage pristine-tar git yasm \
	checkinstall cvs mercurial libexif* \
	libid3tag* libavutil-dev libavcodec-dev \
	libavformat-dev libavdevice-dev \
	libavfilter-dev libavresample-dev \
	libswscale-dev \
	libpostproc-dev uuid-dev libexpat1-dev \
	libsqlite3-dev libmysqlclient-dev \
	libmagic-dev libexif-dev libcurl4-openssl-dev \
	libav-tools ffmpegthumbnailer \
	libtool gcc-7 g++-7  --install-recommends

sudo update-alternatives --install /usr/bin/gcc gcc /usr/bin/gcc-7 60 --slave /usr/bin/g++ g++ /usr/bin/g++-7

git clone https://github.com/gerbera/gerbera.git

mkdir build

cd build

cat << EOF > install-pupnp18-modified.sh
#!/bin/sh
set -ex

unamestr=Linux
if [ "\$unamestr" == 'FreeBSD' ]; then
   extraFlags=""
else
   extraFlags="--prefix=/usr/local"
fi

wget https://github.com/mrjimenez/pupnp/archive/release-1.8.3.tar.gz -O pupnp-1.8.3.tgz
tar -xzvf pupnp-1.8.3.tgz
cd pupnp-release-1.8.3
./bootstrap && ./configure \$extraFlags --enable-ipv6 --enable-reuseaddr && make -j8 LDFLAGS="-pthread" CFLAGS='-D_LARGE_FILE_SOURCE -D_FILE_OFFSET_BITS=64' && sudo make install && sudo ldconfig
EOF

sh install-pupnp18-modified.sh
#sh ../gerbera/scripts/install-pupnp18.sh 
sh ../gerbera/scripts/install-duktape.sh 
sh ../gerbera/scripts/install-taglib111.sh

cmake ../gerbera/ -DWITH_MAGIC=1 -DWITH_MYSQL=0 -DWITH_CURL=1 -DWITH_JS=1 -DWITH_TAGLIB=1 -DWITH_AVCODEC=0 -DWITH_EXIF=1 -DWITH_LASTFM=0 -DWITH_SYSTEMD=0

make -j8

sudo make install

gerbera --create-config > ~/.config/gerbera/config.xml

gerbera






